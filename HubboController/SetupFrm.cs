﻿using BetterHttpClient;
using HubboController.Json;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Tangine.Communication;
using Tangine.Protocol;

namespace HubboController
{
    public partial class SetupFrm : Form
    {
        private SessionService Sessions = new SessionService();
        private HNode ScoutConnection = new HNode();
        private Users Accounts;
        private Random Rand = new Random(Guid.NewGuid().GetHashCode());

        public SetupFrm()
        {
            InitializeComponent();
            Accounts = SessionService.GetSessions();

            var scout = Accounts.users.Where(_ => _.response == "pong").First();
            //var scout = Accounts.users.First();

            txtEmail.Text = scout.email;
            txtPassword.Text = scout.password;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                ScoutConnection.ConnectAsync(txtServer.Text, 8765).Wait();
                ScoutConnection.SendMessageAsync(0, "").Wait();
                ScoutConnection.SendMessageAsync(6, 74392246).Wait();
                ScoutConnection.SendMessageAsync(7, true, 1, 30).Wait();
                Thread.Sleep(300);
                ScoutConnection.SendMessageAsync(4).Wait();

                Thread.Sleep(1000);

                ScoutConnection = new HNode();
                ScoutConnection.ConnectAsync(txtServer.Text, 8765).Wait();
            }
            catch
            {
                MessageBox.Show("Could not connect to bot server!");
                return;
            }

            var scout = Accounts.users.Where(_ => _.response == "pong").First();
            Accounts.users.Remove(scout);

            ScoutConnection.SendMessageAsync(0, scout.username);

            var message = new HMessage(1, "www.habbo.com", RandomString(15));
            message.WriteInteger(3);
            message.WriteString(scout.cookie_ypf);
            message.WriteString("");
            message.WriteString(scout.cookie_session_id.Split('=')[0]);
            message.WriteString(scout.cookie_session_id.Split('=')[1]);
            message.WriteString(scout.cookie_browser_token.Split('=')[0]);
            message.WriteString(scout.cookie_browser_token.Split('=')[1]);

            var proxy = (Proxy)Sessions.ProxyQueue.Dequeue();

            message.WriteString(proxy.ProxyItem.Address.Host);
            message.WriteInteger(proxy.ProxyItem.Address.Port);

            ScoutConnection.SendMessageAsync(message);

            Thread.Sleep(1000);

            new MainFrm(Sessions, ScoutConnection, txtServer.Text, Accounts).Show();
            this.Hide();
        }

        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[Rand.Next(s.Length)]).ToArray());
        }
    }
}
