﻿namespace HubboController
{
    partial class SetupFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpConnectionInfo = new System.Windows.Forms.GroupBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.grpScoutInfo = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.grpConnectionInfo.SuspendLayout();
            this.grpScoutInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpConnectionInfo
            // 
            this.grpConnectionInfo.Controls.Add(this.txtServer);
            this.grpConnectionInfo.Controls.Add(this.lblServer);
            this.grpConnectionInfo.Location = new System.Drawing.Point(12, 12);
            this.grpConnectionInfo.Name = "grpConnectionInfo";
            this.grpConnectionInfo.Size = new System.Drawing.Size(208, 53);
            this.grpConnectionInfo.TabIndex = 0;
            this.grpConnectionInfo.TabStop = false;
            this.grpConnectionInfo.Text = "Connection Info";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(68, 19);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(134, 20);
            this.txtServer.TabIndex = 1;
            this.txtServer.Text = "lonestar.hypermine.com";
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(6, 22);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(41, 13);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "Server:";
            // 
            // grpScoutInfo
            // 
            this.grpScoutInfo.Controls.Add(this.txtPassword);
            this.grpScoutInfo.Controls.Add(this.label1);
            this.grpScoutInfo.Controls.Add(this.txtEmail);
            this.grpScoutInfo.Controls.Add(this.lblEmail);
            this.grpScoutInfo.Location = new System.Drawing.Point(12, 71);
            this.grpScoutInfo.Name = "grpScoutInfo";
            this.grpScoutInfo.Size = new System.Drawing.Size(208, 78);
            this.grpScoutInfo.TabIndex = 1;
            this.grpScoutInfo.TabStop = false;
            this.grpScoutInfo.Text = "Scout Info";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(68, 45);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(134, 20);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.Text = "Richard53";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Password:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(68, 19);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(134, 20);
            this.txtEmail.TabIndex = 1;
            this.txtEmail.Text = "harble@getnada.com";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(6, 22);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "Email:";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(139, 155);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // SetupFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 190);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.grpScoutInfo);
            this.Controls.Add(this.grpConnectionInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupFrm";
            this.Text = "HubboSpam - Setup";
            this.grpConnectionInfo.ResumeLayout(false);
            this.grpConnectionInfo.PerformLayout();
            this.grpScoutInfo.ResumeLayout(false);
            this.grpScoutInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpConnectionInfo;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.GroupBox grpScoutInfo;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnConnect;
    }
}