﻿using BetterHttpClient;
using HubboController.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tangine.Communication;
using Tangine.Protocol;

namespace HubboController
{
    public partial class MainFrm : Form
    {
        private string ServerAddress;
        private SessionService Sessions;
        private HNode ScoutConnection;
        private Users Accounts;
        private Dictionary<string, HNode> Connections;

        public MainFrm(SessionService sessions, HNode scoutConnection, string serverAddress, Users accounts)
        {
            InitializeComponent();
            Sessions = sessions;
            ScoutConnection = scoutConnection;
            ServerAddress = ScoutConnection.EndPoint.Host;
            Accounts = accounts;
            Connections = new Dictionary<string, HNode>();

            rtbNavCat.Lines = File.ReadAllLines("nav_categories.txt");
            rtbPhrases.Lines = File.ReadAllLines("phrases.txt");

            Task.Factory.StartNew(() =>
            {
                foreach (var account in Accounts.users.Where(_ => _.response == "pong"))
                //foreach (var account in Accounts.users)
                {
                        var connection = new HNode();
                    connection.ConnectAsync(ServerAddress, 8765).Wait();
                    connection.SendMessageAsync(0, account.username);

                    var message = new HMessage(1, "www.habbo.com", "");
                    message.WriteInteger(3);
                    message.WriteString(account.cookie_ypf);
                    message.WriteString("");
                    message.WriteString(account.cookie_session_id.Split('=')[0]);
                    message.WriteString(account.cookie_session_id.Split('=')[1]);
                    message.WriteString(account.cookie_browser_token.Split('=')[0]);
                    message.WriteString(account.cookie_browser_token.Split('=')[1]);

                    var proxy = (Proxy)Sessions.ProxyQueue.Dequeue();

                    message.WriteString(proxy.ProxyItem.Address.Host);
                    message.WriteInteger(proxy.ProxyItem.Address.Port);

                    connection.SendMessageAsync(message);

                    Connections.Add(account.username, connection);

                    Thread.Sleep(2000);
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                foreach (var account in Accounts.users)
                {
                    var connection = new HNode();
                    connection.ConnectAsync(ServerAddress, 8765).Wait();
                    connection.SendMessageAsync(0, account.username);

                    var message = new HMessage(1, "www.habbo.com");
                    message.WriteInteger(3);
                    message.WriteString(account.cookie_ypf);
                    message.WriteString("");
                    message.WriteString(account.cookie_session_id.Split('=')[0]);
                    message.WriteString(account.cookie_session_id.Split('=')[1]);
                    message.WriteString(account.cookie_browser_token.Split('=')[0]);
                    message.WriteString(account.cookie_browser_token.Split('=')[1]);

                    connection.SendMessageAsync(message);

                    Connections.Add(account.username, connection);

                    Thread.Sleep(1000);
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void btnUpdatePhrases_Click(object sender, EventArgs e)
        {
            rtbPhrases.Enabled = false;
            File.WriteAllLines("phrases.txt", rtbPhrases.Lines);

            var message = new HMessage(5, String.Join((char)13 + "", rtbPhrases.Lines));

            ScoutConnection.SendMessageAsync(message);
            rtbPhrases.Enabled = true;
        }

        private void btnUpdateNav_Click(object sender, EventArgs e)
        {
            //rtbNavCat.Enabled = false;
            //File.WriteAllLines("nav_categories.txt", rtbNavCat.Lines);

            //var message = new HMessage(3, String.Join((char)13 + "", rtbNavCat.Lines));

            //ScoutConnection.SendMessageAsync(message);
            //rtbNavCat.Enabled = true;

            ScoutConnection.SendMessageAsync(6, (int)numRoomId.Value);
        }

        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnShutdown_Click(object sender, EventArgs e)
        {
            ScoutConnection.SendMessageAsync(11);
        }

        private void chkEnableSpam_CheckedChanged(object sender, EventArgs e)
        {
            ScoutConnection.SendMessageAsync(9, chkEnableSpam.Checked).Wait();
        }

        private void EnableWalk_Changed(object sender, EventArgs e)
        {
            ScoutConnection.SendMessageAsync(7, chkEnableWalk.Checked, (int)numWalkMin.Value, (int)numWalkMax.Value).Wait();
        }

        private void chkEnableFigure_CheckedChanged(object sender, EventArgs e)
        {
            ScoutConnection.SendMessageAsync(12, cmbFigureGender.Text, txtFigureCode.Text);
        }
    }
}