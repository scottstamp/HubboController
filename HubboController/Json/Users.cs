﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubboController.Json
{
    public class Users
    {
        public bool success { get; set; }
        public List<User> users { get; set; }
    }

    public class User
    {
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string cookie_ypf { get; set; }
        public string cookie_browser_token { get; set; }
        public string cookie_session_id { get; set; }
        public string response { get; set; }
        public long timestamp { get; set; }
    }
}
