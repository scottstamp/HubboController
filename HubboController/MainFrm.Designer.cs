﻿namespace HubboController
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpSpamPhrases = new System.Windows.Forms.GroupBox();
            this.chkEnableSpam = new System.Windows.Forms.CheckBox();
            this.btnUpdatePhrases = new System.Windows.Forms.Button();
            this.rtbPhrases = new System.Windows.Forms.RichTextBox();
            this.grpNavCat = new System.Windows.Forms.GroupBox();
            this.btnShutdown = new System.Windows.Forms.Button();
            this.numRoomId = new System.Windows.Forms.NumericUpDown();
            this.btnUpdateNav = new System.Windows.Forms.Button();
            this.rtbNavCat = new System.Windows.Forms.RichTextBox();
            this.grpAutoWalk = new System.Windows.Forms.GroupBox();
            this.chkEnableWalk = new System.Windows.Forms.CheckBox();
            this.numWalkMax = new System.Windows.Forms.NumericUpDown();
            this.numWalkMin = new System.Windows.Forms.NumericUpDown();
            this.grpFigureSettings = new System.Windows.Forms.GroupBox();
            this.chkEnableFigure = new System.Windows.Forms.CheckBox();
            this.cmbFigureGender = new System.Windows.Forms.ComboBox();
            this.txtFigureCode = new System.Windows.Forms.TextBox();
            this.grpSpamPhrases.SuspendLayout();
            this.grpNavCat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoomId)).BeginInit();
            this.grpAutoWalk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWalkMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWalkMin)).BeginInit();
            this.grpFigureSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSpamPhrases
            // 
            this.grpSpamPhrases.Controls.Add(this.chkEnableSpam);
            this.grpSpamPhrases.Controls.Add(this.btnUpdatePhrases);
            this.grpSpamPhrases.Controls.Add(this.rtbPhrases);
            this.grpSpamPhrases.Location = new System.Drawing.Point(12, 12);
            this.grpSpamPhrases.Name = "grpSpamPhrases";
            this.grpSpamPhrases.Size = new System.Drawing.Size(297, 280);
            this.grpSpamPhrases.TabIndex = 1;
            this.grpSpamPhrases.TabStop = false;
            this.grpSpamPhrases.Text = "Spam Phrases";
            // 
            // chkEnableSpam
            // 
            this.chkEnableSpam.AutoSize = true;
            this.chkEnableSpam.Location = new System.Drawing.Point(6, 254);
            this.chkEnableSpam.Name = "chkEnableSpam";
            this.chkEnableSpam.Size = new System.Drawing.Size(89, 17);
            this.chkEnableSpam.TabIndex = 4;
            this.chkEnableSpam.Text = "Enable Spam";
            this.chkEnableSpam.UseVisualStyleBackColor = true;
            this.chkEnableSpam.CheckedChanged += new System.EventHandler(this.chkEnableSpam_CheckedChanged);
            // 
            // btnUpdatePhrases
            // 
            this.btnUpdatePhrases.Location = new System.Drawing.Point(216, 251);
            this.btnUpdatePhrases.Name = "btnUpdatePhrases";
            this.btnUpdatePhrases.Size = new System.Drawing.Size(75, 23);
            this.btnUpdatePhrases.TabIndex = 3;
            this.btnUpdatePhrases.Text = "Update";
            this.btnUpdatePhrases.UseVisualStyleBackColor = true;
            this.btnUpdatePhrases.Click += new System.EventHandler(this.btnUpdatePhrases_Click);
            // 
            // rtbPhrases
            // 
            this.rtbPhrases.Location = new System.Drawing.Point(6, 19);
            this.rtbPhrases.Name = "rtbPhrases";
            this.rtbPhrases.Size = new System.Drawing.Size(285, 226);
            this.rtbPhrases.TabIndex = 2;
            this.rtbPhrases.Text = "";
            // 
            // grpNavCat
            // 
            this.grpNavCat.Controls.Add(this.btnShutdown);
            this.grpNavCat.Controls.Add(this.numRoomId);
            this.grpNavCat.Controls.Add(this.btnUpdateNav);
            this.grpNavCat.Controls.Add(this.rtbNavCat);
            this.grpNavCat.Location = new System.Drawing.Point(315, 12);
            this.grpNavCat.Name = "grpNavCat";
            this.grpNavCat.Size = new System.Drawing.Size(297, 280);
            this.grpNavCat.TabIndex = 2;
            this.grpNavCat.TabStop = false;
            this.grpNavCat.Text = "Navigator Categories";
            // 
            // btnShutdown
            // 
            this.btnShutdown.Location = new System.Drawing.Point(6, 251);
            this.btnShutdown.Name = "btnShutdown";
            this.btnShutdown.Size = new System.Drawing.Size(75, 23);
            this.btnShutdown.TabIndex = 3;
            this.btnShutdown.Text = "Shutdown";
            this.btnShutdown.UseVisualStyleBackColor = true;
            this.btnShutdown.Click += new System.EventHandler(this.btnShutdown_Click);
            // 
            // numRoomId
            // 
            this.numRoomId.Location = new System.Drawing.Point(129, 251);
            this.numRoomId.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numRoomId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numRoomId.Name = "numRoomId";
            this.numRoomId.Size = new System.Drawing.Size(81, 20);
            this.numRoomId.TabIndex = 2;
            this.numRoomId.Value = new decimal(new int[] {
            71911277,
            0,
            0,
            0});
            // 
            // btnUpdateNav
            // 
            this.btnUpdateNav.Location = new System.Drawing.Point(216, 251);
            this.btnUpdateNav.Name = "btnUpdateNav";
            this.btnUpdateNav.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateNav.TabIndex = 1;
            this.btnUpdateNav.Text = "Update";
            this.btnUpdateNav.UseVisualStyleBackColor = true;
            this.btnUpdateNav.Click += new System.EventHandler(this.btnUpdateNav_Click);
            // 
            // rtbNavCat
            // 
            this.rtbNavCat.Location = new System.Drawing.Point(6, 19);
            this.rtbNavCat.Name = "rtbNavCat";
            this.rtbNavCat.Size = new System.Drawing.Size(285, 226);
            this.rtbNavCat.TabIndex = 0;
            this.rtbNavCat.Text = "";
            // 
            // grpAutoWalk
            // 
            this.grpAutoWalk.Controls.Add(this.chkEnableWalk);
            this.grpAutoWalk.Controls.Add(this.numWalkMax);
            this.grpAutoWalk.Controls.Add(this.numWalkMin);
            this.grpAutoWalk.Location = new System.Drawing.Point(12, 298);
            this.grpAutoWalk.Name = "grpAutoWalk";
            this.grpAutoWalk.Size = new System.Drawing.Size(195, 45);
            this.grpAutoWalk.TabIndex = 3;
            this.grpAutoWalk.TabStop = false;
            this.grpAutoWalk.Text = "AutoWalk";
            // 
            // chkEnableWalk
            // 
            this.chkEnableWalk.AutoSize = true;
            this.chkEnableWalk.Location = new System.Drawing.Point(6, 20);
            this.chkEnableWalk.Name = "chkEnableWalk";
            this.chkEnableWalk.Size = new System.Drawing.Size(87, 17);
            this.chkEnableWalk.TabIndex = 4;
            this.chkEnableWalk.Text = "Enable Walk";
            this.chkEnableWalk.UseVisualStyleBackColor = true;
            this.chkEnableWalk.CheckedChanged += new System.EventHandler(this.EnableWalk_Changed);
            // 
            // numWalkMax
            // 
            this.numWalkMax.Location = new System.Drawing.Point(147, 19);
            this.numWalkMax.Name = "numWalkMax";
            this.numWalkMax.Size = new System.Drawing.Size(42, 20);
            this.numWalkMax.TabIndex = 1;
            this.numWalkMax.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numWalkMax.ValueChanged += new System.EventHandler(this.EnableWalk_Changed);
            // 
            // numWalkMin
            // 
            this.numWalkMin.Location = new System.Drawing.Point(99, 19);
            this.numWalkMin.Name = "numWalkMin";
            this.numWalkMin.Size = new System.Drawing.Size(42, 20);
            this.numWalkMin.TabIndex = 0;
            this.numWalkMin.ValueChanged += new System.EventHandler(this.EnableWalk_Changed);
            // 
            // grpFigureSettings
            // 
            this.grpFigureSettings.Controls.Add(this.txtFigureCode);
            this.grpFigureSettings.Controls.Add(this.cmbFigureGender);
            this.grpFigureSettings.Controls.Add(this.chkEnableFigure);
            this.grpFigureSettings.Location = new System.Drawing.Point(213, 298);
            this.grpFigureSettings.Name = "grpFigureSettings";
            this.grpFigureSettings.Size = new System.Drawing.Size(260, 45);
            this.grpFigureSettings.TabIndex = 4;
            this.grpFigureSettings.TabStop = false;
            this.grpFigureSettings.Text = "Figure Settings";
            // 
            // chkEnableFigure
            // 
            this.chkEnableFigure.AutoSize = true;
            this.chkEnableFigure.Location = new System.Drawing.Point(6, 20);
            this.chkEnableFigure.Name = "chkEnableFigure";
            this.chkEnableFigure.Size = new System.Drawing.Size(91, 17);
            this.chkEnableFigure.TabIndex = 0;
            this.chkEnableFigure.Text = "Enable Figure";
            this.chkEnableFigure.UseVisualStyleBackColor = true;
            this.chkEnableFigure.CheckedChanged += new System.EventHandler(this.chkEnableFigure_CheckedChanged);
            // 
            // cmbFigureGender
            // 
            this.cmbFigureGender.FormattingEnabled = true;
            this.cmbFigureGender.Items.AddRange(new object[] {
            "M",
            "F"});
            this.cmbFigureGender.Location = new System.Drawing.Point(103, 18);
            this.cmbFigureGender.Name = "cmbFigureGender";
            this.cmbFigureGender.Size = new System.Drawing.Size(37, 21);
            this.cmbFigureGender.TabIndex = 1;
            this.cmbFigureGender.Text = "M";
            // 
            // txtFigureCode
            // 
            this.txtFigureCode.Location = new System.Drawing.Point(146, 18);
            this.txtFigureCode.Name = "txtFigureCode";
            this.txtFigureCode.Size = new System.Drawing.Size(108, 20);
            this.txtFigureCode.TabIndex = 2;
            this.txtFigureCode.Text = "hr-115-42.hd-180-1.ch-255-64.lg-280-73.sh-300-64";
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 355);
            this.Controls.Add(this.grpFigureSettings);
            this.Controls.Add(this.grpAutoWalk);
            this.Controls.Add(this.grpNavCat);
            this.Controls.Add(this.grpSpamPhrases);
            this.Name = "MainFrm";
            this.Text = "HubboController";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.grpSpamPhrases.ResumeLayout(false);
            this.grpSpamPhrases.PerformLayout();
            this.grpNavCat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numRoomId)).EndInit();
            this.grpAutoWalk.ResumeLayout(false);
            this.grpAutoWalk.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWalkMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWalkMin)).EndInit();
            this.grpFigureSettings.ResumeLayout(false);
            this.grpFigureSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSpamPhrases;
        private System.Windows.Forms.Button btnUpdatePhrases;
        private System.Windows.Forms.RichTextBox rtbPhrases;
        private System.Windows.Forms.GroupBox grpNavCat;
        private System.Windows.Forms.Button btnUpdateNav;
        private System.Windows.Forms.RichTextBox rtbNavCat;
        private System.Windows.Forms.NumericUpDown numRoomId;
        private System.Windows.Forms.Button btnShutdown;
        private System.Windows.Forms.CheckBox chkEnableSpam;
        private System.Windows.Forms.GroupBox grpAutoWalk;
        private System.Windows.Forms.CheckBox chkEnableWalk;
        private System.Windows.Forms.NumericUpDown numWalkMax;
        private System.Windows.Forms.NumericUpDown numWalkMin;
        private System.Windows.Forms.GroupBox grpFigureSettings;
        private System.Windows.Forms.TextBox txtFigureCode;
        private System.Windows.Forms.ComboBox cmbFigureGender;
        private System.Windows.Forms.CheckBox chkEnableFigure;
    }
}

