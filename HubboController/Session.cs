﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using BetterHttpClient;
using System.Web.Script.Serialization;
using System.IO;

namespace HubboController
{
    public class Session
    {
        public string Email { get; private set; }
        public string Password { get; private set; }
        public CookieContainer CookieJar { get; private set; }
        public Dictionary<string, string> Cookies { get; private set; }

        private HttpClient Client;
        private TwoCaptchaClient CaptchaClient;
        private JavaScriptSerializer JsonSerializer;

        public Session(string email, string password)
        {
            Email = email;
            Password = password;
            Cookies = new Dictionary<string, string>();
            CookieJar = new CookieContainer();

            Client = new HttpClient(new Proxy("mapluntu.hypermine.com", 9001, ProxyTypeEnum.Socks))
            {
                UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
                Encoding = Encoding.UTF8,
                Accept = "application/json, text/plain, */*",
                AcceptEncoding = "gzip, deflate",
                Cookies = CookieJar
            };

            CaptchaClient = new TwoCaptchaClient("a8bda7bde08f688785d0976df6d03834");
            JsonSerializer = new JavaScriptSerializer();

            InitializeSession();
        }

        private void InitializeSession()
        {
            var response = Client.Get("https://www.habbo.com");
            var matches = new Regex(@"setCookie\('(.*)', '(.*)', ([0-9]+)\);").Match(response);
            CookieJar.Add(new Cookie(matches.Groups[1].Value, matches.Groups[2].Value, "/", "www.habbo.com"));
        }

        public void AttemptLogin()
        {
            var response = Client.UploadString("https://www.habbo.com/api/public/authentication/login",
                JsonSerializer.Serialize(new Auth { email = Email, password = Password }));

            if (response.Contains("invalid-captcha"))
            {
                string token;
                if (CaptchaClient.SolveRecaptchaV2("6LdhyucSAAAAAKNhbY53azV2gZul4DcD8Xo111yp", "https://www.habbo.com/",
                    "lonestar.hypermine.com:9001", ProxyType.SOCKS5, out token))
                {
                    response = Client.UploadString("https://www.habbo.com/api/public/authentication/login",
                        JsonSerializer.Serialize(new Auth {
                            email = Email,
                            password = Password,
                            captchaToken = token
                        }));
                }
            }
        }

        public void SaveCookies()
        {
            List<string> cookies = new List<string>();
            foreach (var cookie in GetCookies())
            {
                cookies.Add(cookie.Key + "=" + cookie.Value);
            }

            File.WriteAllLines("cookies\\" + Email.Replace('@', '.'), cookies);
        }

        public bool LoadCookies()
        {
            var exists = File.Exists("cookies\\" + Email.Replace('@', '.'));
            if (exists)
                foreach (var cookie in File.ReadAllLines("cookies\\" + Email.Replace('@', '.')))
                    CookieJar.Add(new Cookie(cookie.Split('=')[0], cookie.Split('=')[1], "/", "www.habbo.com"));

            return exists;
        }

        public Dictionary<string, string> GetCookies()
        {
            var cookies = new Dictionary<string, string>();

            foreach (Cookie cookie in CookieJar.GetCookies(new Uri("https://www.habbo.com")))
                cookies.Add(cookie.Name, cookie.Value);

            return cookies;
        }
    }

    public class Auth
    {
        public string email { get; set; }
        public string password { get; set; }
        public string captchaToken { get; set; }
    }

    public class MessageResponse
    {
        public string message { get; set; }
        public bool captcha { get; set; }
    }
}
