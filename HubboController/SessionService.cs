﻿using BetterHttpClient;
using HubboController.Json;
using System.Collections;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace HubboController
{
    public class SessionService
    {
        private static HttpClient Client = new HttpClient();
        private static JavaScriptSerializer JsonSerializer = new JavaScriptSerializer();
        public Queue ProxyQueue = new Queue();

        public SessionService()
        {
            ProxyQueue.Enqueue(new Proxy("198.46.179.8", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.46.181.78", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.123.36", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.168.36.9", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.94.85", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.183.174", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.41.124.46", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.144.102", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.41.126.30", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.162.206", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.243.9", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.7.9", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.172.4.214", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.87.21", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.41.127.50", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.148.61", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.53.126", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.53.120", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.23.207.91", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.227.27.3", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.46.181.79", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.228.178.29", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.245.94", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.208.32", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.241.13", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.228.178.28", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.19.3", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.244.103", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.144.99", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.46.181.57", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.102.193", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.11.45", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.51.3", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.131.86", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.63.2", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.58.53", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.41.125.57", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.62.3", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.83.32", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.137.133", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.53.125", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.227.26.3", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.26.162", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.210.153", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.211.69.230", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.193.88", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.198.123", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.232.114", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.242.9", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.228.178.30", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.193.113", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.103.82", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.211.68.232", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.210.18", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.194.206", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.240.16", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.187.27", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.150.216.66", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.181.117", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.172.47.79", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.8.10", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.65.53", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.94.213", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.175.20.2", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.90.204", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.90.141", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.133.78", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.196.68", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.26.149", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.22.63", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("45.33.159.27", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.150.217.36", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.46.223.4", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.172.108", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.172.47.42", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.172.109", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.90.197", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.151.227.222", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("155.254.168.7", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.69.103", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.42.56", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.234.81", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.44.205.16", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.160.251.14", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.160.238.198", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.46.180.69", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.23.133.151", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.233.210", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.198.206", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.175.152", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.175.118", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.202.240.83", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.202.172.2", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.199.93", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.235.43", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.235.151", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.93", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.88.56.215", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.202.240.84", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.233.180", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.232.29", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.97", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.63.180", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.202.240.82", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.234.194", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.98", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.235.152", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.246.191.2", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.233.179", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.223.96.9", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.223.99.5", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.198.144", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.234.39", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.198.177", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.235.146", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.199.99", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.198.135", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.174.143", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.234.69", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.199.104", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.174.147", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.218.199.112", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.63.4", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.91", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.23.249.102", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("155.254.179.238", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.59.7", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.160.157", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.94.184", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.106", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.105", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.203.59", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.169.217", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.241.49", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.249.5", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.203.63", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.227.233.133", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.218.37", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("45.33.152.120", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.243.45", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.241.50", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.94", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.179.195", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.253.26", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.241.48", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("45.33.152.102", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.163.99", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.195.22", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.160.154", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.183.121", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.243.46", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("216.158.218.55", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.200.243.47", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.163.55", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.238.112", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.183.119", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.160.156", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.191.236", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.238.111", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("185.137.183.165", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("179.61.151.136", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("45.59.24.62", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.163.54", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("184.174.75.168", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("179.61.145.180", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("67.220.254.40", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("184.174.73.244", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.179.202", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("45.59.24.43", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.251.81.66", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("185.117.20.211", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("191.101.68.201", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("192.3.162.33", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("184.174.74.164", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.97.60", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.160.234.100", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.188.230", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.98.169.213", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("185.117.21.149", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("208.110.61.81", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("208.110.62.53", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("208.110.60.93", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.228.166.113", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("191.101.74.217", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("191.101.74.223", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("179.61.145.183", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("179.61.151.161", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("191.101.68.202", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("172.245.97.49", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.63.153", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.95.61.82", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("173.228.167.114", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.232.75", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("104.168.88.65", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("107.172.242.74", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("23.94.233.77", 26836, ProxyTypeEnum.Socks));
            ProxyQueue.Enqueue(new Proxy("198.46.245.166", 26836, ProxyTypeEnum.Socks));
        }

        public static Users GetSessions()
        {
            var response = Client.Get("http://lonestar.hypermine.com/cookies.php?action=get");
            return JsonSerializer.Deserialize<Users>(response);
        }

        public string GetSsoToken(User user)
        {
            var sso = "";
            var proxy = (Proxy)ProxyQueue.Dequeue();
            var cookiejar = new CookieContainer();
            cookiejar.Add(new Cookie(user.cookie_ypf, "", "/", "www.habbo.com"));
            cookiejar.Add(new Cookie("browser_token", user.cookie_browser_token, "/", "www.habbo.com"));
            cookiejar.Add(new Cookie("session.id", user.cookie_session_id, "/", "www.habbo.com"));

            var client = new HttpClient(new Proxy("mapluntu.hypermine.com", 9001, ProxyTypeEnum.Socks))
            {
                UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
                Encoding = Encoding.UTF8,
                Accept = "application/json, text/plain, */*",
                AcceptEncoding = "gzip, deflate",
                Cookies = cookiejar
            };

            var response = Client.Get("https://www.habbo.com/api/ssotoken");

            if (response.Contains("ssoToken"))
                sso = response.Split('"')[3];

            return sso;
        }
    }
}
